"""
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""

import numpy as np


class Transform:
    def __init__(self, X, Y, L, P, robust=False):
        """
        grid.transform class : finds the affine transform to go from map coordinates to
         line/point or inline/crossline
        :param X: numpy vector X coordinates
        :param Y: numpy vector Y coordinates
        :param L: numpy vector Line Number
        :param P: numpy vector Point Number
        :param robust: boolean scalar (False), if True, split randomly to make several
        regressions and take median parameters
        """
        xy = np.c_[X, Y, X * 0 + 1]
        lp = np.c_[L, P, L * 0 + 1]
        self.mat_lp2xy = np.linalg.lstsq(lp, xy)[0]
        self.mat_xy2lp = np.linalg.lstsq(xy, lp)[0]

    def xy2lp(self, X, Y, rounding=True):
        """
        grid.xy2lp : transforms from X,Y coordinates to line/point or inline/crossline
        :param X: numpy vector X coordinates
        :param Y: numpy vector Y coordicates
        :param round: boolean scalar (True): if True, rounds the output to the nearest integer
        :return: L,P: numpy vectors of line/point or inline/crossline
        """
        res = np.c_[X, Y, X * 0 + 1].dot(self.mat_xy2lp)
        if rounding:
            L = np.round(res[:, 0])
            P = np.round(res[:, 1])
            return L, P
        else:
            return res[:, 0], res[:, 1]

    def lp2xy(self, L, P):
        """
        grid.lp2xy : transforms from line/point or inline/crossline to X,Y coordinates
        :param L,P: numpy vectors of line/point or inline/crossline
        :return: X,Y: numpy vectors of geographical coordinates
        """
        res = np.c_[L, P, L * 0 + 1].dot(self.mat_lp2xy)
        return res[:, 0], res[:, 1]
