# README #

### What is this repository for? ###

* A collection of Python modules applied to seismic
* [Learn Markdown](https://bitbucket.org/tutorials/markdowndemo)

### How do I get set up? ###

* Scientific libraries are used, ie. numpy, scipy etc
* PyQt5 handles GUI
* I recommend using Anaconda or similar python scientific distribution.

### MIT License. ###