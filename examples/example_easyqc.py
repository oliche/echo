from pathlib import Path

import pyqtgraph as pg

import io_.segy as segy
import easyqc

# read the segy
segy_test_path = Path('/datadisk/Echo/tests/segy')
segy_file = "/datadisk/Echo/tests/segy/00063102.sgy"
w, fh, th, thbin, sr = segy.read(segy_file)
# pyuic5 -x easyqc.ui -o easyqc_ui.py

segy_file = "/datadisk/Echo/tests/segy/Stack/bcat_5r.sgy"
sr = segy.SegyReader(segy_file)
w, th, thbin = sr.read_from_to(50000, 55000)


eqc1 = easyqc.viewdata(w, h=th, title='eqc1')
# eqc2 = easyqc.viewdata(w, title='eqc2')


# from PyQt5 import QtWidgets, QtCore
# import pyqtgraph as pg
##


# interactive mode only works in Ipython with the magic command: %gui qt

# find eqc objects from anywhere (prerequisite to link figures)
# app = QtWidgets.QApplication.instance()
# eqcs = [w for w in app.topLevelWidgets() if isinstance(w, easyqc.EasyQC)]
#
