from pathlib import Path
import ctypes
import os
import numpy as np

# Try to locate the .so file in the same directory as this file

if os.name == 'nt':
    _file = 'sampleconversion.dll'
else:
    _file = 'sampleconversion_lib.so'
_path = Path(__file__).parent.joinpath(_file)
_mod = ctypes.cdll.LoadLibrary(_path)

def ibm2ieee_float(W):
    pt = W.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
    _mod.ibm_to_float(pt, pt, W.size, 0)
    W = W.view(np.float32)
    return W


# def ibm2ieee_float(ibm):
#     sign = (1 - 2 *np.float32( ibm >> 31 & 0x01))
#     sign *= (ibm & 0x00ffffff) / float(pow(2, 24))
#     sign *= np.float32((16 ** (( ibm >> 24 & 0x7f) - 64)))
#     return sign
