"""
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License

import trace_indices
ind=np.array([2000,5])
ind=np.array([5,2,3,4, 1, 20, 11 ,12,10])
ind=5

fromto_tr, ordre =trace_bucket_from_trace_indices(ind)
byt_offset, nbyt = byte_offset_from_trace_bucket(fromto_tr,3600,240,10000)
byt_offset, nbyt, ordre = bytes_offsets_from_trace_indices(ind,3600,240,10000) """

import numpy as np


def trace_bucket_from_trace_indices(ind):
    """ fromTrace_toTrace, ordre = trace_bucket_from_trace_indices(TraceIndexes)   """
    #ind=np.array([5,2,3,4, 1, 20, 11 ,12,10])
    # make sure the input is an array
    if ~(ind.__class__ is np.array):
        ind = np.array(ind)
    # make sure the input has at least 2 elements
    if ind.size == 1:
        fromto = np.c_[ind, ind]
        ordre = np.array(0)
        return fromto, ordre
    # get the sorting order to ouput traces in requested order
    ordre = np.argsort(ind)
    ind.sort()
    fromto = np.nonzero(np.diff(ind) != 1)[0]
    #import pdb; pdb.set_trace()
    fromto = ind[np.c_[np.r_[0, fromto + 1], np.r_[fromto, len(ind) - 1]]]
    return fromto, ordre


def byte_offset_from_trace_bucket(fromto_tr, fhlength, thlength, trlength):
    """ byt_offset, count= byte_offset_from_trace_bucket(ind, fhlength, thlength, trlength)"""
    byt_offset = (fromto_tr[:, 0] - 1) * (thlength + trlength) + fhlength
    count = (np.diff(fromto_tr, 1) + 1)  # * (thlength+trlength)
    return byt_offset, count


def bytes_offsets_from_trace_indices(ind, fhlength, thlength, trlength):
    """ byt_offset, count, ordre = bytes_offsets_from_trace_indices(ind,fhlength,thlength,trlength)"""
    fromto_tr, ordre = trace_bucket_from_trace_indices(ind)
    byt_offset, nbyt = byte_offset_from_trace_bucket(
        fromto_tr, fhlength, thlength, trlength)
    return byt_offset, nbyt, ordre
