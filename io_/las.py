"""
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""
import numpy as np
import re
import pandas as pd
import warnings


def read(las_file):
    with open(las_file, 'r') as fid:
        mystr = fid.read()
    # the las file is a text file with sections delimited by ~
    mystr = mystr.split('~')
    # Loop over sections to interpret headers
    Header = []
    for ind in range(len(mystr)):
        if len(mystr[ind]) == 0:
            continue
        section = mystr[ind].split('\n')
        if section[0][0] == 'V':          # 'Version Information Section':
            Header.append(_read_header(section))
        elif section[0][0] == 'W':        # 'Well Information Section':
            Header.append(_read_header(section))
        elif section[0][0] == 'C':      # 'Curve Information Section':
            Curve = _read_header(section)
        elif section[0][0] == 'A':  # 'ASCII Log Data Section':
            # Cette partie est environ 3 fois plus lente que le sscanf de
            # matlab (probablement du C) boarf
            Data = np.fromstring(mystr[ind][mystr[ind].find(
                '\n') + 1:], dtype=np.float, count=-1, sep=" ")
            Data.shape = [int(Data.size / len(Curve)), int(len(Curve))]
            Data[Data == -999.0] = np.NAN
            # found some examples like this one also...
            Data[Data == -999.25] = np.NAN
        else:
            warnings.warn("Section Not Supported: " + section[0] + str(ind))
            continue
    # reformat the data as a pandas dataframe
    Fieldnames = [x[0][0].strip() for x in Curve]
    Data = pd.DataFrame(data=Data, columns=Fieldnames)
    return Data


def _read_header(section):
    """
    This is an example of a header
    Out[81]: 'VERS.          2.0                                     : CWLS LOG ASCII Standard'
    """
    interp = []
    for ind in range(1, len(section)):
        if len(section[ind]) == 0:
            continue
        if section[ind][0] == '#':
            continue       # the pound indicates comments on the las files
        interp.append(re.findall(r'(\S*.|\S*\s*.)\.(\S*)\s*(\S*)', section[ind])[0:2])
    return interp
