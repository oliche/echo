"""
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""
import numpy as np
import pandas as pd

# %% Read


def readR(sps_file, *args):
    # sps.readR(rps_file)
    # sps.readR(rps_file,rev)
    DF, Header = readS(sps_file, *args)
    return DF, Header


def readS(sps_file, *args):
    # sps.readS(sps_file)
    # sps.readS(sps_file,rev)
    if len(args) >= 1:
        rev = args[0]
        DF, Header = read(sps_file, rev=rev, file_type='S')
    else:
        DF, Header = read(sps_file, file_type='S')
    return DF, Header


def readX(sps_file, *args):
    # sps.readX(sps_file)
    # sps.readX(sps_file,rev)
    if len(args) >= 1:
        rev = args[0]
        DF, Header = read(sps_file, rev=rev, file_type='X')
    else:
        DF, Header = read(sps_file, file_type='X')
    return DF, Header


def readVAPS(sps_file):
    DF, Header = read(sps_file, file_type='V')
    return DF, Header


def readSEGP1(sps_file):
    # This wrapper computes X/Y/Lat/Long according to NAM survey subcontracors conventions:
    # 1) Lat/Long are in DDMMSSSS format in the SP1
    # 2) Feet or meters are expressed in decifeet or decimeters in the SP1
    # the output is in decimal degrees and feet/meters
    DF, Header = read(sps_file, file_type='P')
    # Convert the Lat/Long to decimal degrees
    try:
        def str2ddec(x): return float(
            x[:-6]) + float(x[-6:-4]) / 60 + float(x[-4:]) / 100 / 3600
        DF.Lat = DF.Lat.apply(str2ddec) * np.sign((DF.LatN == 'N') - 0.5)
        DF.Long = DF.Long.apply(str2ddec) * np.sign((DF.LongE == 'E') - 0.5)
    except BaseException:
        pass
    if 'X' in DF:
        DF.X = DF.X / 10
    if 'Y' in DF:
        DF.Y = DF.Y / 10
    return DF, Header


def writeSEGP1(sps_out, Data):
    # sps.writeSEGP1(sps_out, Data)
    # wrapper function to handle SEGP1 specificities
    Data.X *= 10
    Data.Y *= 10
    def ddec2str(x): return "%02.0f" % np.floor(abs(x)) + "%02.0f" % np.floor(
        (abs(x) % 1) * 60) + "%04.0f" % ((((abs(x) % 1) * 60) % 1) * 6000)
    Data.Lat = Data.Lat.apply(ddec2str) + Data.LatN
    Data.Long = Data.Long.apply(ddec2str) + Data.LongE
    write(sps_out, Data, file_type='P')


def read(sps_file, **kwargs):
    # [DataFrame Header]=sps.read(sps_file)
    # [DataFrame Header]=sps.read(... , rev=2.1, file_type='S')

    # parse input arguments
    rev = kwargs.get('rev', 0)
    file_type = kwargs.get('file_type', '')

    # read the file in one stroke
    fid = open(sps_file, 'r')
    A = fid.readlines()
    fid.close()
    # get the header as a list of lines (for now probably the most convenient
    # format)
    nh = sum([l[0] == 'H' for l in A])
    Header = A[0:nh]
    nh = sum([l[0] == 'H' for l in A])
    A = A[nh:]
    Data = {}

    # if no information is provided, try to auto detect some info about the
    # format (revision and file_type)
    if file_type == "":
        file_type = GetFileType(A)
    if rev == 0:
        rev = GetRevision(A, file_type)

    # headers are not labeled correctly in the SP1 format coming from survey
    # companies
    if file_type == "P":
        lrecord = np.round(np.median([len(x) for x in A if len(x) > 50]))
        nheaders = len(np.nonzero(np.array([len(x) for x in A]) != lrecord)[0])
        Header = Header + A[:nheaders + 1]
        A = A[nheaders:]
        A = [x for x in A if len(x) == lrecord]

    # get the format description dictionary
    sps_f = sps_formats(file_type, rev)
    # parse the mega list to a dictionary of numpy arrays
    for ind in range(len(sps_f)):
        typ = sps_f[ind][3]
        # strips the spaces, if all empty, jump to the next field
        L = [x[sps_f[ind][1] - 1: sps_f[ind][2]].strip() for x in A]
        if sum([len(x) for x in L]) == 0:
            continue
        # convert to desired type
        L = [typ(x) if len(x) > 0 else typ() for x in L]
        Data[sps_f[ind][0]] = np.asarray(L, dtype=typ)
    # make a dataframe object out of the thing
    Data = pd.DataFrame(data=Data, columns=Data.keys())
    return Data, Header


def write(sps_out, Data, **kwargs):
    # sps.write(... , rev=2.1, file_type='S')
    # need to add a Header as an option

    # parse input arguments
    rev = kwargs.get('rev', 0)
    file_type = kwargs.get('file_type', '')

    # get the dataframe format
    sps_f = sps_formats(file_type, rev)
    fields = Data.keys().tolist()
    # remove fields without data from the format list
    sps_f = [v for v in sps_f if any([x == v[0] for x in fields])]
    # init a numpy chararray
    mystr = np.zeros([Data.count()[0], 80], dtype='c')
    mystr[:, :] = ' '
    mystr[:, -1] = '\n'
    # loop over each line (very slow!!)
    for ind in range(Data.count()[0]):
        # make sure the field exists in the data frame, otherwise put spaces
        for ff in sps_f:
            nchar = ff[2] - ff[1] + 1
            mystr[ind][ff[1] - 1:ff[2]
                       ] = ff[4](ff[5].format(Data[ff[0]][ind]), nchar)[0:nchar]
    # write the numpy chararray in a single stroke
    fid = open(sps_out, 'w')
    mystr.tofile(fid)
    fid.close()


def sps_formats(file_type, rev):
    # this code is genrated from a VBA macro in SPS_Formats_Python.xlsm
    if file_type == 'S' or file_type == 'R':
        # SPS
        if rev == 1:
            sps_format = [
                [
                    'Record_ID', 1, 1, str, str.rjust, "'S' or 'R'"], [
                    'SL', 2, 17, float, str.ljust, "F16.1 "], [
                    'SN', 18, 25, float, str.rjust, "F8.1 "], [
                    'SI', 26, 26, int, str.rjust, "I1   "], [
                        'Point_code', 27, 28, str, str.rjust, "A2   "], [
                            'Static_correction', 29, 32, int, str.rjust, "I4   "], [
                                'Point_depth', 33, 36, float, str.rjust, "F4.1 "], [
                                    'Seismic_datum', 37, 40, int, str.rjust, "I4   "], [
                                        'Uphole_time', 41, 42, int, str.rjust, "I2   "], [
                                            'Water_depth', 43, 46, float, str.rjust, "F4.1 "], [
                                                'X', 47, 55, float, str.rjust, "F9.1 "], [
                                                    'Y', 56, 65, float, str.rjust, "F10.1"], [
                                                        'Z', 66, 71, float, str.rjust, "F6.1 "], [
                                                            'JulianDay', 72, 74, int, str.rjust, "I3   "], [
                                                                'HHMMSS', 75, 80, int, str.zfill, "3A2  "], ]  # SPS
        # SPS2.1
        if rev >= 2:
            sps_format = [
                [
                    'Record_ID', 1, 1, str, str.rjust, "'S' or 'R'"], [
                    'SL', 2, 11, float, str.rjust, "F10.2"], [
                    'SN', 12, 21, float, str.rjust, "F10.2"], [
                    'SI', 24, 24, int, str.rjust, "I1   "], [
                        'Point_code', 25, 26, str, str.ljust, "A2   "], [
                            'Static_correction', 27, 30, int, str.rjust, "I4   "], [
                                'Point_depth', 31, 34, float, str.rjust, "F4.1 "], [
                                    'Seismic_datum', 35, 38, int, str.rjust, "I4   "], [
                                        'Uphole_time', 39, 40, int, str.rjust, "I2   "], [
                                            'Water_depth', 41, 46, float, str.rjust, "F6.1 "], [
                                                'X', 47, 55, float, str.rjust, "F9.1 "], [
                                                    'Y', 56, 65, float, str.rjust, "F10.1"], [
                                                        'Z', 66, 71, float, str.rjust, "F6.1 "], [
                                                            'JulianDay', 72, 74, int, str.rjust, "I3   "], [
                                                                'HHMMSS', 75, 80, int, str.zfill, "3I2  "], ]  # SPS2.1
    elif file_type == 'X':
        # XPS
        if rev == 1:
            sps_format = [
                [
                    'Record_ID', 1, 1, str, str.rjust, "'X'"], [
                    'TapeNumber', 2, 7, int, str.ljust, "3A2"], [
                    'FFID', 8, 11, int, str.rjust, "I4 "], [
                    'FFIDInc', 12, 12, int, str.rjust, "I1 "], [
                        'InstCode', 13, 13, int, str.rjust, "I1 "], [
                            'SL', 14, 29, float, str.ljust, "F16.1 "], [
                                'SN', 30, 37, float, str.rjust, "F8.1 "], [
                                    'SI', 38, 38, int, str.rjust, "I1 "], [
                                        'Low_channel', 39, 42, int, str.rjust, "I4 "], [
                                            'High_channel', 43, 46, int, str.rjust, "I4 "], [
                                                'Channel_inc', 47, 47, int, str.rjust, "I1 "], [
                                                    'RL', 48, 63, float, str.ljust, "F16.1 "], [
                                                        'Low_rec', 64, 71, float, str.rjust, "F8.1 "], [
                                                            'High_rec', 72, 79, float, str.rjust, "F8.1 "], [
                                                                'Rec_index', 80, 80, float, str.rjust, "I1 "], ]  # XPS
        # XPS2.1
        if rev >= 2:
            sps_format = [
                [
                    'Record_ID', 1, 1, str, str.rjust, "'X'"], [
                    'TapeNumber', 2, 7, int, str.rjust, "3A2"], [
                    'FFID', 8, 15, int, str.rjust, "I8"], [
                    'FFIDInc', 16, 16, int, str.rjust, "I1 "], [
                        'InstCode', 17, 17, str, str.rjust, "A1"], [
                            'SL', 18, 27, float, str.rjust, "F10.2"], [
                                'SN', 28, 37, float, str.rjust, "F10.2"], [
                                    'SI', 38, 38, int, str.rjust, "I1 "], [
                                        'Low_channel', 39, 43, int, str.rjust, "I5"], [
                                            'High_channel', 44, 48, int, str.rjust, "I5"], [
                                                'Channel_inc', 49, 49, int, str.rjust, "I1 "], [
                                                    'RL', 50, 59, float, str.rjust, "F10.2"], [
                                                        'Low_rec', 60, 69, float, str.rjust, "F10.2"], [
                                                            'High_rec', 70, 79, float, str.rjust, "F10.2"], [
                                                                'Rec_index', 80, 80, int, str.rjust, "I1 "], ]  # XPS2.1
    elif file_type == 'V' or file_type == 'A':
        # VAPS
        sps_format = [
            [
                'Record_ID', 1, 1, str, str.rjust, "A1   "], [
                'SPSL', 2, 17, float, str.rjust, "4A4"], [
                'SPSN', 18, 25, float, str.rjust, "2A4"], [
                    'SPSI', 26, 26, int, str.rjust, "I1   "], [
                        'Fleet', 27, 27, int, str.rjust, "1-W"], [
                            'Dsd', 28, 29, int, str.rjust, "I2   "], [
                                'Drive', 30, 32, float, str.rjust, "I3   "], [
                                    'MeanPhase', 33, 36, float, str.rjust, "I4   "], [
                                        'MaxPhase', 37, 40, float, str.rjust, "I4   "], [
                                            'MeanDist', 41, 42, float, str.rjust, "I2   "], [
                                                'MaxDist', 43, 44, float, str.rjust, "I2   "], [
                                                    'MeanAmp', 45, 46, float, str.rjust, "I2   "], [
                                                        'MaxAmp', 47, 49, float, str.rjust, "I3   "], [
                                                            'Gs', 50, 52, float, str.rjust, "I3   "], [
                                                                'Gv', 53, 55, float, str.rjust, "I3   "], [
                                                                    'X', 56, 64, float, str.rjust, "F9.1 "], [
                                                                        'Y', 65, 74, float, str.rjust, "F10.1"], [
                                                                            'Z', 75, 80, float, str.rjust, "F6.1 "], [
                                                                                'ShotNumberVibro', 82, 86, int, str.rjust, "I5   "], [
                                                                                    'AcquisitionNumber', 87, 88, int, str.rjust, "I2   "], [
                                                                                        'Fleet', 89, 90, int, str.rjust, "I2  "], [
                                                                                            'VibStatusCode', 91, 92, int, str.rjust, "I2   "], [
                                                                                                'MassWarning', 94, 96, str, str.rjust, "space or W   "], [
                                                                                                    'PlateWarning', 100, 105, str, str.rjust, "space or W   "], [
                                                                                                        'ForceOverload', 106, 106, str, str.rjust, "space or F"], [
                                                                                                            'PressureOverload', 107, 107, str, str.rjust, "space or P"], [
                                                                                                                'MassOverload', 108, 108, str, str.rjust, "space or M"], [
                                                                                                                    'ValveOverload', 109, 109, str, str.rjust, "space of V"], [
                                                                                                                        'ExcitationOverload', 110, 110, str, str.rjust, "space or E"], [
                                                                                                                            'StackingFold', 111, 112, int, str.rjust, "I2"], [
                                                                                                                                'ComputationDomain', 113, 113, str, str.rjust, "A1"], [
                                                                                                                                    'VEversion', 114, 117, str, str.rjust, "A4"], [
                                                                                                                                        'JulianDay', 118, 120, int, str.rjust, "I3   "], [
                                                                                                                                            'HHMMSS', 121, 126, int, str.zfill, "3I2"], [
                                                                                                                                                'Hdop', 127, 130, float, str.rjust, "F4.1"], [
                                                                                                                                                    'TB_Gps', 131, 150, np.int64, str.rjust, "A20"], ]  # VAPS
    elif file_type == 'P':
        # SEGP1
        sps_format = [
            [
                'Record_ID', 1, 1, str, str.rjust, "A"], [
                'Line_name', 2, 17, int, str.ljust, "4A4"], [
                'Point_number', 18, 25, int, str.rjust, "I8"], [
                    'Point_index', 26, 26, str, str.rjust, "I1"], [
                        'Lat', 27, 34, str, str.rjust, "A"], [
                            'LatN', 35, 35, str, str.rjust, "A"], [
                                'Long', 36, 44, str, str.rjust, "A"], [
                                    'LongE', 45, 45, str, str.rjust, "A"], [
                                        'X', 46, 53, float, str.rjust, "F8.2"], [
                                            'Y', 54, 61, float, str.rjust, "F8.2"], [
                                                'Z', 62, 67, float, str.rjust, "F5.2"], [
                                                    'Year', 68, 69, int, str.rjust, "I2"], [
                                                        'JulianDay', 70, 72, int, str.rjust, "I03"], [
                                                            'HHMMSS', 73, 78, str, str.zfill, "I06"], ]  # SEGP1
        # this creates the writing format string
    equiformat = {'float': 'f', 'int': 'd', 'str': 's'}
    for ind in range(len(sps_format)):
        strformat = equiformat[sps_format[ind][3].__name__]
        # if float, 2 digits if the field is more than 6 digits long
        if (strformat == 'f'):
            strformat = sps_format[ind][5].strip().split("F")[-1] + strformat
        strformat = '{:' + strformat + '}'
        sps_format[ind][5] = strformat
    return sps_format


def GetRevision(A, file_type):
    # test for revision if it wasn't provided : the justification of
    # line/points is ok
    rev = 1
    # s-file
    if file_type == 'S':
        if sum([x[1] == " " for x in A]) / len(A) > 0.9:
            rev = 2
    # x-file
    if file_type == 'R':
        if sum([x[26] == " " for x in A]) / len(A) < 0.1:
            rev = 2
    # the end
    return rev


def GetFileType(A):
    # works for S,R,X,Vaps
    # automatic detection fails for sp1 files
    file_type = A[-1][0]
    return file_type
