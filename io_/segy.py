"""
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""
from abc import ABC, abstractmethod
from pathlib import Path
import struct
import ctypes
import os
import numpy as np

# Try to locate the .so file in the same directory as this file
if os.name == 'nt':
    _file = 'sampleconversion.dll'
else:
    _file = 'sampleconversion_lib.so'
_path = Path(__file__).parent.joinpath(_file)
_mod = ctypes.cdll.LoadLibrary(_path)


def ibm2ieee_float(w):
    pt = w.ctypes.data_as(ctypes.POINTER(ctypes.c_int32))
    _mod.ibm_to_float(pt, pt, w.size, 0)
    w = w.view(np.float32)
    return w


class Reader(ABC):

    def __init__(self, seismic_file):
        self.seismic_file = seismic_file

    def read_from_to(self, first, last):
        return self.read_from_trace_indices(np.r_[first:last + 1])

    def read(self):
        return self.read_from_trace_indices(np.r_[1:int(self.ntr + 1)])

    @abstractmethod
    def read_from_trace_indices(self, ind):
        pass

    @staticmethod
    def trace_bucket_from_trace_indices(ind):
        """ fromTrace_toTrace, ordre = trace_bucket_from_trace_indices(TraceIndexes)   """
        # ind=np.array([5,2,3,4, 1, 20, 11 ,12,10])
        # make sure the input is an array
        if ~(ind.__class__ is np.array):
            ind = np.array(ind)
        # make sure the input has at least 2 elements
        if ind.size == 1:
            fromto = np.c_[ind, ind]
            ordre = np.array(0)
            return fromto, ordre
        # get the sorting order to ouput traces in requested order
        ordre = np.argsort(ind)
        ind.sort()
        fromto = np.nonzero(np.diff(ind) != 1)[0]
        # import pdb; pdb.set_trace()
        fromto = ind[np.c_[np.r_[0, fromto + 1], np.r_[fromto, len(ind) - 1]]]
        return fromto, ordre

    @staticmethod
    def byte_offset_from_trace_bucket(fromto_tr, fhlength, thlength, trlength):
        """ byt_offset, count= byte_offset_from_trace_bucket(ind, fhlength, thlength, trlength)"""
        byt_offset = (fromto_tr[:, 0] - 1) * (thlength + trlength) + fhlength
        count = (np.diff(fromto_tr, 1) + 1)
        return byt_offset, count

    @staticmethod
    def bytes_offsets_from_trace_indices(ind, fhlength, thlength, trlength):
        """ byt_offset, count, ordre = bytes_offsets_from_trace_indices(ind,fhlength,thlength,trlength)"""
        fromto_tr, ordre = Reader.trace_bucket_from_trace_indices(ind)
        byt_offset, nbyt = Reader.byte_offset_from_trace_bucket(
            fromto_tr, fhlength, thlength, trlength)
        return byt_offset, nbyt, ordre


class SegyReader(Reader):
    """Base class for segy object"""
    fhsize = 3600
    thsize = 240

    def __init__(self, seismic_file):
        super(SegyReader, self).__init__(seismic_file)
        # read file header
        self.fh, self.text_head = self.read_file_header(self.seismic_file)
        # get number of samples as a class attribute
        self.ns = self.fh['Nech']
        # get the samplesize in bytes (almost always 4 but...)
        self.sample_size_byte = 4 - (self.fh['DataSampleFormat'] == 3) * 2
        # compute number of traces
        nbyt = os.path.getsize(seismic_file)
        self.ntr = (nbyt - self.fhsize) / (self.thsize + self.fh['Nech'] * 4)

    def read_from_trace_indices(self, ind):
        ntr = len(ind)
        nelements = int(self.ns + self.thsize / self.sample_size_byte)
        # look at the sample file format to pre-interpret samples while reading
        dtypes_segy = ['>I', '>i', '>h', '', '>f']
        thisfile_dtype = dtypes_segy[self.fh['DataSampleFormat'] - 1]
        # this computes the most optimized way to hop around traces
        byt_offset, count, ordre = self.bytes_offsets_from_trace_indices(
            ind, self.fhsize, self.thsize, self.ns * self.sample_size_byte)
        # Pre-assign arrays before looping over them
        data = np.zeros((ntr, nelements), thisfile_dtype)
        # open the file and loop over the byte offsets
        fid = open(self.seismic_file, 'rb')
        curtr = int(0)
        for nchk in range(len(byt_offset)):
            fid.seek(int(byt_offset[nchk]))
            bytes2read = int((nelements) * count[nchk])
            data[curtr:curtr + int(count[nchk]),
                 :] = np.fromfile(fid,
                                  dtype=thisfile_dtype,
                                  count=bytes2read).reshape((int(count[nchk]),
                                                             nelements))
        fid.close()
        # dissociate the trace header from the trace data
        thbin, data = np.split(data, [np.int(240 / self.sample_size_byte)], 1)
        # pack the arrays for future efficiency
        thbin = np.ascontiguousarray(thbin)
        data = np.ascontiguousarray(data)
        # create a uint8 view of TH so it can be interpreted
        thbin = thbin.view(np.uint8)
        # now interpret the data if it is IBM format (other types come straight
        # up)
        if self.fh['DataSampleFormat'] == 1:  # IBM float 32
            data = ibm2ieee_float(data)
        # at last, interpret the trace header
        th = self.interpret_th(thbin)
        # set the endianness to native format
        if data.dtype.byteorder != '=':
            data = data.astype(data.dtype.newbyteorder('='))
        return data, th, thbin

    def interpret_th(self, thbin):
        nfields = 91
        th = np.zeros([thbin.shape[0], nfields])
        th[:, 0] = np.copy(thbin[:, 0:4]).view('>i').squeeze()
        th[:, 1] = np.copy(thbin[:, 4:8]).view('>i').squeeze()
        th[:, 2] = np.copy(thbin[:, 8:12]).view('>i').squeeze()
        th[:, 3] = np.copy(thbin[:, 12:16]).view('>i').squeeze()
        th[:, 4] = np.copy(thbin[:, 16:20]).view('>i').squeeze()
        th[:, 5] = np.copy(thbin[:, 20:24]).view('>i').squeeze()
        th[:, 6] = np.copy(thbin[:, 24:28]).view('>i').squeeze()
        th[:, 7] = np.copy(thbin[:, 28:30]).view('>h').squeeze()
        th[:, 8] = np.copy(thbin[:, 30:32]).view('>h').squeeze()
        th[:, 9] = np.copy(thbin[:, 32:34]).view('>h').squeeze()
        th[:, 10] = np.copy(thbin[:, 34:36]).view('>h').squeeze()
        th[:, 11] = np.copy(thbin[:, 36:40]).view('>i').squeeze()
        th[:, 12] = np.copy(thbin[:, 40:44]).view('>i').squeeze()
        th[:, 13] = np.copy(thbin[:, 44:48]).view('>i').squeeze()
        th[:, 14] = np.copy(thbin[:, 48:52]).view('>i').squeeze()
        th[:, 15] = np.copy(thbin[:, 52:56]).view('>i').squeeze()
        th[:, 16] = np.copy(thbin[:, 56:60]).view('>i').squeeze()
        th[:, 17] = np.copy(thbin[:, 60:64]).view('>i').squeeze()
        th[:, 18] = np.copy(thbin[:, 64:68]).view('>i').squeeze()
        th[:, 19] = np.copy(thbin[:, 68:70]).view('>h').squeeze()
        th[:, 20] = np.copy(thbin[:, 70:72]).view('>h').squeeze()
        th[:, 21] = np.copy(thbin[:, 72:76]).view('>i').squeeze()
        th[:, 22] = np.copy(thbin[:, 76:80]).view('>i').squeeze()
        th[:, 23] = np.copy(thbin[:, 80:84]).view('>i').squeeze()
        th[:, 24] = np.copy(thbin[:, 84:88]).view('>i').squeeze()
        th[:, 25] = np.copy(thbin[:, 88:90]).view('>h').squeeze()
        th[:, 26] = np.copy(thbin[:, 90:92]).view('>h').squeeze()
        th[:, 27] = np.copy(thbin[:, 92:94]).view('>h').squeeze()
        th[:, 28] = np.copy(thbin[:, 94:96]).view('>h').squeeze()
        th[:, 29] = np.copy(thbin[:, 96:98]).view('>h').squeeze()
        th[:, 30] = np.copy(thbin[:, 98:100]).view('>h').squeeze()
        th[:, 31] = np.copy(thbin[:, 100:102]).view('>h').squeeze()
        th[:, 32] = np.copy(thbin[:, 102:104]).view('>h').squeeze()
        th[:, 33] = np.copy(thbin[:, 104:106]).view('>h').squeeze()
        th[:, 34] = np.copy(thbin[:, 106:108]).view('>h').squeeze()
        th[:, 35] = np.copy(thbin[:, 108:110]).view('>h').squeeze()
        th[:, 36] = np.copy(thbin[:, 110:112]).view('>h').squeeze()
        th[:, 37] = np.copy(thbin[:, 112:114]).view('>h').squeeze()
        th[:, 38] = np.copy(thbin[:, 114:116]).view('>h').squeeze()
        th[:, 39] = np.copy(thbin[:, 116:118]).view('>h').squeeze()
        th[:, 40] = np.copy(thbin[:, 118:120]).view('>h').squeeze()
        th[:, 41] = np.copy(thbin[:, 120:122]).view('>h').squeeze()
        th[:, 42] = np.copy(thbin[:, 122:124]).view('>h').squeeze()
        th[:, 43] = np.copy(thbin[:, 124:126]).view('>h').squeeze()
        th[:, 44] = np.copy(thbin[:, 126:128]).view('>h').squeeze()
        th[:, 45] = np.copy(thbin[:, 128:130]).view('>h').squeeze()
        th[:, 46] = np.copy(thbin[:, 130:132]).view('>h').squeeze()
        th[:, 47] = np.copy(thbin[:, 132:134]).view('>h').squeeze()
        th[:, 48] = np.copy(thbin[:, 134:136]).view('>h').squeeze()
        th[:, 49] = np.copy(thbin[:, 136:138]).view('>h').squeeze()
        th[:, 50] = np.copy(thbin[:, 138:140]).view('>h').squeeze()
        th[:, 51] = np.copy(thbin[:, 140:142]).view('>h').squeeze()
        th[:, 52] = np.copy(thbin[:, 142:144]).view('>h').squeeze()
        th[:, 53] = np.copy(thbin[:, 144:146]).view('>h').squeeze()
        th[:, 54] = np.copy(thbin[:, 146:148]).view('>h').squeeze()
        th[:, 55] = np.copy(thbin[:, 148:150]).view('>h').squeeze()
        th[:, 56] = np.copy(thbin[:, 150:152]).view('>h').squeeze()
        th[:, 57] = np.copy(thbin[:, 152:154]).view('>h').squeeze()
        th[:, 58] = np.copy(thbin[:, 154:156]).view('>h').squeeze()
        th[:, 59] = np.copy(thbin[:, 156:158]).view('>h').squeeze()
        th[:, 60] = np.copy(thbin[:, 158:160]).view('>h').squeeze()
        th[:, 61] = np.copy(thbin[:, 160:162]).view('>h').squeeze()
        th[:, 62] = np.copy(thbin[:, 162:164]).view('>h').squeeze()
        th[:, 63] = np.copy(thbin[:, 164:166]).view('>h').squeeze()
        th[:, 64] = np.copy(thbin[:, 166:168]).view('>h').squeeze()
        th[:, 65] = np.copy(thbin[:, 168:170]).view('>h').squeeze()
        th[:, 66] = np.copy(thbin[:, 170:172]).view('>h').squeeze()
        th[:, 67] = np.copy(thbin[:, 172:174]).view('>h').squeeze()
        th[:, 68] = np.copy(thbin[:, 174:176]).view('>h').squeeze()
        th[:, 69] = np.copy(thbin[:, 176:178]).view('>h').squeeze()
        th[:, 70] = np.copy(thbin[:, 178:180]).view('>h').squeeze()
        th[:, 71] = np.copy(thbin[:, 180:184]).view('>i').squeeze()
        th[:, 72] = np.copy(thbin[:, 184:188]).view('>i').squeeze()
        th[:, 73] = np.copy(thbin[:, 188:192]).view('>i').squeeze()
        th[:, 74] = np.copy(thbin[:, 192:196]).view('>i').squeeze()
        th[:, 75] = np.copy(thbin[:, 196:200]).view('>i').squeeze()
        th[:, 76] = np.copy(thbin[:, 200:202]).view('>h').squeeze()
        th[:, 77] = np.copy(thbin[:, 202:204]).view('>h').squeeze()
        th[:, 78] = np.copy(thbin[:, 204:208]).view('>i').squeeze()
        th[:, 79] = np.copy(thbin[:, 208:210]).view('>h').squeeze()
        th[:, 80] = np.copy(thbin[:, 210:212]).view('>h').squeeze()
        th[:, 81] = np.copy(thbin[:, 212:214]).view('>h').squeeze()
        th[:, 82] = np.copy(thbin[:, 214:216]).view('>h').squeeze()
        th[:, 83] = np.copy(thbin[:, 216:218]).view('>h').squeeze()
        th[:, 84] = np.copy(thbin[:, 218:222]).view('>i').squeeze()
        th[:, 85] = np.copy(thbin[:, 222:224]).view('>h').squeeze()
        th[:, 86] = np.copy(thbin[:, 224:228]).view('>i').squeeze()
        th[:, 87] = np.copy(thbin[:, 228:230]).view('>h').squeeze()
        th[:, 88] = np.copy(thbin[:, 230:232]).view('>h').squeeze()
        th[:, 89] = np.copy(thbin[:, 232:236]).view('>i').squeeze()
        th[:, 90] = np.copy(thbin[:, 236:240]).view('>i').squeeze()
        dtype = np.dtype([('TraceSequenceLine', 'Float64'),
                          ('TraceSequenceFile', 'Float64'),
                          ('FFID', 'Float64'),
                          ('TraceNumber', 'Float64'),
                          ('EnergySourcePoint', 'Float64'),
                          ('CDP', 'Float64'),
                          ('TraceSequenceCDP', 'Float64'),
                          ('TraceID', 'Float64'),
                          ('VerticalStack', 'Float64'),
                          ('HorizontalStack', 'Float64'),
                          ('DataUse', 'Float64'),
                          ('Offset', 'Float64'),
                          ('Zrcv', 'Float64'),
                          ('Zshot', 'Float64'),
                          ('SourceDepth', 'Float64'),
                          ('RcvDatumElevation', 'Float64'),
                          ('ShotDatumElevation', 'Float64'),
                          ('ShotWaterDepth', 'Float64'),
                          ('RcvWaterDepth', 'Float64'),
                          ('ElevationScalar', 'Float64'),
                          ('CoordinatesScalar', 'Float64'),
                          ('Xshot', 'Float64'),
                          ('Yshot', 'Float64'),
                          ('Xrcv', 'Float64'),
                          ('Yrcv', 'Float64'),
                          ('CoordinateUnits', 'Float64'),
                          ('WeatheringVelocity', 'Float64'),
                          ('SubWeatheringVelocity', 'Float64'),
                          ('SourceUpholeTime', 'Float64'),
                          ('RcvUpholeTime', 'Float64'),
                          ('ShotStatic', 'Float64'),
                          ('RcvStatic', 'Float64'),
                          ('TotalStatic', 'Float64'),
                          ('LagTimeA', 'Float64'),
                          ('LagTimeB', 'Float64'),
                          ('DelayRecordingTime', 'Float64'),
                          ('MuteTimeStart', 'Float64'),
                          ('MuteTimeEnd', 'Float64'),
                          ('Nech', 'Float64'),
                          ('Si', 'Float64'),
                          ('FieldInstrumentGainTypeCode', 'Float64'),
                          ('InstrumentGainConstant', 'Float64'),
                          ('InstrumentInitialGain', 'Float64'),
                          ('Correlated', 'Float64'),
                          ('Fb', 'Float64'),
                          ('Fe', 'Float64'),
                          ('SL', 'Float64'),
                          ('SweepType', 'Float64'),
                          ('TapB', 'Float64'),
                          ('TapE', 'Float64'),
                          ('TaperType', 'Float64'),
                          ('AliasFilterFreq', 'Float64'),
                          ('AliasFilterSlope', 'Float64'),
                          ('NotchFilterFreq', 'Float64'),
                          ('NotchFilterSlope', 'Float64'),
                          ('LowCutFreq', 'Float64'),
                          ('HighCutFreq', 'Float64'),
                          ('LowCutSlope', 'Float64'),
                          ('HighCutSlope', 'Float64'),
                          ('Year', 'Float64'),
                          ('JulianDay', 'Float64'),
                          ('Hour', 'Float64'),
                          ('Minute', 'Float64'),
                          ('Second', 'Float64'),
                          ('TimeBasis', 'Float64'),
                          ('MilliSecOfSec', 'Float64'),
                          ('GeophoneGroupNumberRoll1', 'Float64'),
                          ('GeophoneGroupNumberFirstTraceOrigField', 'Float64'),
                          ('GeophoneGroupNumberLastTraceOrigField', 'Float64'),
                          ('GapSize', 'Float64'),
                          ('OverTravel', 'Float64'),
                          ('Xcdp', 'Float64'),
                          ('Ycdp', 'Float64'),
                          ('InLine', 'Float64'),
                          ('CrossLine', 'Float64'),
                          ('ShotPoint', 'Float64'),
                          ('ShotPointScalar', 'Float64'),
                          ('TraceValueMeasurementUnit', 'Float64'),
                          ('TransductionConstantMantissa', 'Float64'),
                          ('TransductionConstantPower', 'Float64'),
                          ('TransductionUnit', 'Float64'),
                          ('TraceIdentifier', 'Float64'),
                          ('ScalarTraceHeader', 'Float64'),
                          ('SourceType', 'Float64'),
                          ('SourceEnergyDirectionMantissa', 'Float64'),
                          ('SourceEnergyDirectionExponent', 'Float64'),
                          ('SourceMeasurementMantissa', 'Float64'),
                          ('SourceMeasurementExponent', 'Float64'),
                          ('SourceMeasurementUnit', 'Float64'),
                          ('VSP_ShotLine', 'Float64'),
                          ('UnassignedInt2', 'Float64')])
        th = th.view(dtype)
        return th

    @staticmethod
    def read_file_header(segy_file):
        """ FH,TextHead = segy.utils.readFileHeader(segy_file)   """
        # % Read File Header
        fid = open(segy_file, 'rb')
        ebcdic = fid.read(3200)
        fhbin = fid.read(400)
        fid.close()
        # Interpret EBDCIC
        text_head = ebcdic.decode('EBCDIC-CP-BE')
        # Parse binary file header
        fh = {'Task': struct.unpack('>i', fhbin[0:4])[0]}
        fh['Line'] = struct.unpack('>i', fhbin[4:8])[0]
        fh['Reel'] = struct.unpack('>i', fhbin[8:12])[0]
        fh['DataTracePerEnsemble'] = struct.unpack('>h', fhbin[12:14])[0]
        fh['AuxiliaryTracePerEnsemble'] = struct.unpack('>h', fhbin[14:16])[0]
        fh['Si'] = struct.unpack('>h', fhbin[16:18])[0]
        fh['SiOrig'] = struct.unpack('>h', fhbin[18:20])[0]
        fh['Nech'] = struct.unpack('>H', fhbin[20:22])[0]
        fh['NechOrig'] = struct.unpack('>H', fhbin[22:24])[0]
        fh['DataSampleFormat'] = struct.unpack('>h', fhbin[24:26])[0]
        fh['EnsembleFold'] = struct.unpack('>h', fhbin[26:28])[0]
        fh['TraceSorting'] = struct.unpack('>h', fhbin[28:30])[0]
        fh['VerticalSumCode'] = struct.unpack('>h', fhbin[30:32])[0]
        fh['SweepFrequencyStart'] = struct.unpack('>h', fhbin[32:34])[0]
        fh['SweepFrequencyEnd'] = struct.unpack('>h', fhbin[34:36])[0]
        fh['SweepLength'] = struct.unpack('>H', fhbin[36:38])[0]
        fh['SweepType'] = struct.unpack('>h', fhbin[38:40])[0]
        fh['SweepChannel'] = struct.unpack('>h', fhbin[40:42])[0]
        fh['SweepTaperlengthStart'] = struct.unpack('>h', fhbin[42:44])[0]
        fh['SweepTaperLengthEnd'] = struct.unpack('>h', fhbin[44:46])[0]
        fh['TaperType'] = struct.unpack('>h', fhbin[46:48])[0]
        fh['CorrelatedDataTraces'] = struct.unpack('>h', fhbin[48:50])[0]
        fh['BinaryGain'] = struct.unpack('>h', fhbin[50:52])[0]
        fh['AmplitudeRecoveryMethod'] = struct.unpack('>h', fhbin[52:54])[0]
        fh['MeasurementSystem'] = struct.unpack('>h', fhbin[54:56])[0]
        fh['ImpulseSignalPolarity'] = struct.unpack('>h', fhbin[56:58])[0]
        fh['VibratoryPolarityCode'] = struct.unpack('>h', fhbin[58:60])[0]
        fh['Unassigned1'] = struct.unpack('>h', fhbin[60:62])[0]
        fh['SegyFormatRevisionNumber'] = struct.unpack('>h', fhbin[300:302])[0]
        fh['FixedLengthTraceFlag'] = struct.unpack('>h', fhbin[302:304])[0]
        fh['NumberOfExtTextualHeaders'] = struct.unpack('>h', fhbin[304:306])[0]
        fh['Unassigned2'] = struct.unpack('>h', fhbin[306:308])[0]
        return fh, text_head


def read(segy_file, *args, **kwargs):
    sr = SegyReader(segy_file)
    w, th, thbin = sr.read(*args, **kwargs)
    return w, sr.fh, th, thbin, sr
