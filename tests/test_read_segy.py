"""
Display the first 10,000 traces of a segy seismic section
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""
from pathlib import Path

import numpy as np
import matplotlib.pyplot as plt
import pyqtgraph as pg

import io_.segy as segy


def test_read_01():
    pass
    # read the segy
    segy_test_path = Path('/datadisk/Echo/tests/segy')
    segy_file = "/datadisk/Echo/tests/00063102.sgy"
    w, fh, th, thbin, sr = segy.read(segy_file)

    # read the first 10k traces
    # ind=np.r_[1:10000]
    # Data,TH,THbin = io.segy.utils.SegyIO.read_from_trace_indices(segy_reader, ind)

    # display using matplotlib

    # plt.imshow(w)
    # display using pyqtgraph

    im = pg.image(w)
    # pg.QAPP.exec_()
