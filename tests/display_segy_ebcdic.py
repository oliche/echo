"""
Instantiate a segy reader and launch the EBCDIC editor
# MIT License
# Copyright(c) 2017 Olivier Winter
# https: // en.wikipedia.org / wiki / MIT_License
"""

import graphics.view_ebcdic
import segy.utils
# read the segy
segy_file=r"E:\owinter\MATLAB\TESTS\DATA\sgy\Stack\bcat_5r.sgy"
segy_reader=segy.utils.SegyIO(segy_file)
# show the ebcdic
win = graphics.view_ebcdic.show(segy_reader=segy_reader)
win.show()
