import os
import sps
chem = r'E:\2017\ZZ_CHICKASHA\TURNPIKE'
sp1_rcv = chem + os.sep + 'Turnpike trimmed to Chickasha plus SE ext RCV.sp1'
sp1_src = chem + os.sep + 'Turnpike trimmed to Chickasha plus SE ext SRC.sp1'

##
R = sps.readSEGP1(sp1_rcv)[0]
S = sps.readSEGP1(sp1_src)[0]

import matplotlib.pyplot as plt
plt.plot(R.X, R.Y, 'b.')
plt.axis('equal')
plt.plot(S.X, S.Y,'r.')
##

import numpy as np
X = R.X
Y = R.Y
L = R.Line_name
P = R.Point_number

xy = np.c_[X,Y,X*0+1]
lp = np.c_[L,P,L*0+1]


np.linalg.lstsq(xy,lp)